package datatypes;

/**
 * Created by Myung on 12/15/2017.
 */

public class Frame {

    private String mDuration;
    private String mImage;

    public Frame(String duration, String image) {
        this.mDuration = duration;
        this.mImage = image;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String mDuration) {
        this.mDuration = mDuration;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        this.mImage = image;
    }

}
