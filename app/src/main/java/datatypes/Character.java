package datatypes;

import java.util.ArrayList;

/**
 * Created by Myung on 12/15/2017.
 */

public class Character {


    private String mAnnotation;
    private String mPortrait;
    private ArrayList<Move> mNormals;
    private ArrayList<Move> mSpecials;

    public Character() {
    }

    public String getAnnotation() {
        return mAnnotation;
    }

    public void setAnnotation(String mAnnotation) {
        this.mAnnotation = mAnnotation;
    }

    public String getPortrait() {
        return mPortrait;
    }

    public void setPortrait(String mPortrait) {
        this.mPortrait = mPortrait;
    }

    public ArrayList<Move> getNormals() {
        return mNormals;
    }

    public void setNormals(ArrayList<Move> mNormals) {
        this.mNormals = mNormals;
    }

    public ArrayList<Move> getSpecials() {
        return mSpecials;
    }

    public void setSpecials(ArrayList<Move> mSpecials) {
        this.mSpecials = mSpecials;
    }

}
