package datatypes;

import com.gcrd.stfameviewer.Util;

import java.util.ArrayList;

/**
 * Created by Myung on 12/15/2017.
 */

public class Move {

    private ArrayList<Util.Inputs> mInput;
    private ArrayList<Frame> mFrames;
    private String mAnnotation;

    public Move(String annotation) {
        this.mAnnotation = annotation;
    }

    public ArrayList<Util.Inputs> getInput() {
        return mInput;
    }

    public void setInput(ArrayList<Util.Inputs> mInput) {
        this.mInput = mInput;
    }

    public ArrayList<Frame> getFrames() {
        return mFrames;
    }

    public void setFrames(ArrayList<Frame> mFrames) {
        this.mFrames = mFrames;
    }

    public String getAnnotation() {
        return mAnnotation;
    }

    public void setAnnotation(String mAnnotation) {
        this.mAnnotation = mAnnotation;
    }

}

